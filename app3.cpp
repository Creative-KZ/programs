//
//  main.cpp
//  pro3
//
//  Created by Борис Красильников on 18.12.14.
//  Copyright (c) 2014 Борис Красильников. All rights reserved.
//


#include <cstdio>

const int NMAX = 100;

struct Persons
{
    int index;
    bool status;
};

int main()
{
    int i, j, k, n, m;
    Persons a[NMAX];
    printf("Enter the number of persons, n: ");
    scanf("%i", &n);
    printf("Enter the number of output human, m: ");
    scanf("%i", &m);
    
    
    for (i = 0 ; i < n ; i++)
    {
        a[i].index = i + 1;
        a[i].status = 1;
    }
    k = 0;
    while (n > 1)
        for(i = 0; i < n; i++)
        {
            k++;
            if (k % m == 0)
            {
                for(j = i; j < n - 1; j++)
                    a[j] = a[j + 1];
                n--;
                i--;
                k = 0;
            }
        }
    printf("The number of this person: %i",a[0].index);
    getchar();
    return 0;
}
