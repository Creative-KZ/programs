//
//  main.cpp
//  app7
//
//  Created by Борис Красильников on 26.12.14.
//  Copyright (c) 2014 Boris. All rights reserved.
//

#include <iostream>
#include "conio.h"
#include "stdio.h"

#include "rational.h"

int main()
{
    setlocale(LC_ALL, "russian");
    
    rational r1;
    std::cout << "Введите числитель и знаменатель" << std::endl;
    std::cin >> r1;
    
    std::cout << "Число в общем виде" << r1.to_double() << std::endl;
    
    rational r2(10, 23);
    rational r3;
    
    r3 = r1 + r2;
    std::cout << "r1 + r2 = " << r3 << std::endl;
    
    r3 = r1 * r2;
    std::cout << "r1 * r2 = " << r3 << std::endl;
    
    r3 = r1 / r2;
    std::cout << "r1 / r2 = " << r3 << std::endl;
    
    r3 = r1 - r2;
    std::cout << "r1 - r2 = " << r3 << std::endl;
    
    system("pause");
    return 0;
}