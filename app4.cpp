
#include <iostream>
#include <fstream>

using namespace std;

struct Jumpers
{
    char name[30];
    int attempts[3];
    float average;
};

int main()
{
    int i, j;
    char filename[20];
    char data[256];
    Jumpers a[3], t;
    cout << "File name? ";
    cin >> filename;
    ifstream f;
    f.open(filename);
    if (f){
        while (!f.eof())
        {
            //считываем строку из файла
            f.getline(data, 256);
            i = 0;
            //выделяем из строки фамилию спортсмена
            while (!(data[i] >= '0' && data[i] <= '9'))
            {
                t.name[i] = data[i];
                i++;
            }
            t.name[i - 1] = '\0';
            //выделяем из строки попытки спортсмена
            for(j = 0; j < 3; j++)
            {
                t.attempts[j] = 0;
                while ((data[i] >= '0' && data[i] <= '9'))
                {
                    t.attempts[j] = t.attempts[j] * 10 + data[i] - '0';
                    i++;
                }
                i++;
            }
            //вычисляем средний балл среди трех попыток
            t.average = (t.attempts[0] + t.attempts[1] + t.attempts[2]) / 3;
            //распределяем спортсменов по местам основываясь на среднем балле
            if (a[0].average < t.average)
            {
                a[2] = a[1];
                a[1] = a[0];
                a[0] = t;
            }
            else
                if (a[1].average < t.average)
                {
                    a[2] = a[1];
                    a[1] = t;
                }
                else
                    if (a[2].average < t.average)
                        a[2] = t;
        }
        f.close();
        //выводим результат на экран
        for(i = 0; i < 3; i++)
            cout << a[i].name << " " << a[i].attempts[0] << " " << a[i].attempts[1] << " " << a[i].attempts[2] << endl;
    }
    else
        cout << "Error on open file.";
    
    getchar();
    return 0;
}

