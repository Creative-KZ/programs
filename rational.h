
#include <iostream>

class rational
{
    friend std::istream &operator>> (std::istream&, rational&);
    friend std::ostream &operator<< (std::ostream&, rational&);
    
public:
    rational();
    rational(int, int);
    rational(const rational&);
    ~rational();
    
    bool operator=(const rational&);
    int get_m();
    int get_n();
    double to_double();
    
    rational operator*(const rational&);
    rational operator/(const rational&);
    rational operator+(const rational&);
    rational operator-(const rational&);
    
private:
    int m, n;
};