
//  app7
//
//  Created by Борис Красильников on 26.12.14.
//  Copyright (c) 2014 Boris. All rights reserved.
//

#include "rational.h"

bool rational::operator=(const rational& r)
{

    m=r.m;
    n=r.n;
    return true;
    
}

rational::rational()
{
    m = 1;
    n = 1;
}

rational::rational(int m, int n)
{
    this->m = m;
    this->n = n;
}

rational::rational(const rational& r)
{
    m = r.m;
    n = r.n;
}

rational::~rational()
{
    
}

int rational::get_m()	{	return m;	};
int rational::get_n()	{	return n;	}
double rational::to_double()	{ return ((double)m / (double)n); }

std::istream &operator>> (std::istream& is, rational& r)
{
    is.clear();
    is >> r.m >> r.n;
    return is;
}

std::ostream &operator<< (std::ostream& os, rational& r)
{
    os << r.m << "/" << r.n << std::endl;
    return os;
}

rational rational::operator*(const rational& r)
{
    rational r1(m * r.m, n * r.n);
    return r1;
}

rational rational::operator/(const rational& r)
{
    rational r1(m * r.n, n * r.m);
    return r1;
}

rational rational::operator+(const rational& r)
{
    int m1, n1;
    if (n == r.n)
    {
        m1 = m + r.m;
        n1 = n;
    }
    else
    {
        m1 = (m * r.n) + (r.m * n);
        n1 = (n * r.n);
    }
    rational r1(m1, n1);
    return r1;
}

rational rational::operator-(const rational& r)
{
    int m1, n1;
    if (n == r.n)
    {
        m1 = m - r.m;
        n1 = n;
    }
    else
    {
        m1 = (m * r.n) - (r.m * n);
        n1 = (n * r.n);
    }
    rational r1(m1, n1);
    return r1;
}