//
//  main.cpp
//  pro5
//
//  Created by Борис Красильников on 18.12.14.
//  Copyright (c) 2014 Борис Красильников. All rights reserved.
//

#include <iostream>
#include <cmath>

int count, result;

int main(int argc, char* argv[])
{
    int a;
    std::cin >> a;
    while (a > 0)
    {
        if (a % 2 == 0)
        {
            if (count == 0)
            {
                result = (a % 10);
            }
            else
            {
                result += (a % 10) * std::pow(10, count);
            }
            count++;
        }
        a /= 10;
    }
    std::cout << result << std::endl;
    
    return 0;
}